              ВСЕГО ФИНИШ        НОВЫХ  НОВ.ФИН.  %Ф/Н  %Н/ВСЕ  %НФ/ВСЕ  %РЕЦ/НФ %РЕЦ/НОВ.
    2003в       70    25    35%    70     25      35%    100%      35%     84%     60%
    2003о       73    61    83%    57     45      78%     78%      61%     71%     73%
    2004в      131    41    31%    87     25      28%     66%      19%     88%     77%
    2004о      324   219    67%   242    164      67%     74%      50%     62%     63%
    2005в      298    88    29%   171     32      18%     57%      10%     65%     61%
    2005о      470   279    59%   270    140      51%     57%      29%     72%     60%
    2006в      471   218    46%   236     83      35%     50%      17%     80%     69%
    2006о      769   587    76%   403    270      66%     52%      35%     65%     62%
    2007в      720   366    50%   303    121      39%     42%      16%     68%     59%
    2007о     1040   290    27%   485     95      19%     46%       9%     68%     54%
    2008в      879   426    48%   323    109      33%     36%      12%     65%     57%
    2008о     1031   465    45%   403    124      30%     39%      12%     67%     58%
    2009в     1586   606    38%   609    149      24%     38%       9%     60%     38%
    2009о      888   374    42%   292     88      30%     32%       9%     60%     50%
    2010в     1005   390    38%   318     83      26%     31%       8%     48%     46%
    2010о      941   491    52%   348    136      39%     36%      14%     61%     54%
    2011в      957   625    65%   307    173      56%     32%      18%     49%     49%
    2011о     1040   646    62%   339    165      48%     32%      15%     64%     54%
    2012в     1542   733    47%   528    212      40%     34%      13%     57%     44%
    2012о     1362   438    32%   414     77      18%     30%       5%     54%     43%
    2013в     1383   589    42%   342     87      25%     24%       6%     47%     42%
    2013о     1431   627    43%   459    139      30%     32%       9%     49%     37%
    2014в     1856   904    48%   575    192      33%     30%      10%      0%      0%


ВСЕГО    – общее количество участников, вышедших на старт
ФИНИШ    – общее количество финишировавших участников
НОВЫХ    – количество участников, которые до этого не участвовали ни в одном ММБ
НОВ.ФИН. – количество участников, которые до этого не участвовали ни в одном ММБ и успешно финишировали
%Н/ВСЕ – процент участников, которые попали на ММБ впервые к общему числу участников этапа
%НФ/ВСЕ  – процент участников, которые попали на ММБ впервые и финишировали к общему числу участников этапа
%РЕЦ/НФ  – процент новичков, которые пошли на ММБ повторно к числу финишировавших новичков
%РЕЦ/НОВ.– процент новичков, которые пошли на ММБ повторно к общему числу новичков

