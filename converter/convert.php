<?php

require dirname(__FILE__).'/config.php';
require dirname(__FILE__).'/parser.php';

$parser = new MMBParser($config);
$parser->run();