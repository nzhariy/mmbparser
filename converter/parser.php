<?php

class MMBParser {
    /**
     * @var MMBParserConfig
     */
    protected $config;
    protected $users;
    protected $teams;
    protected $matched_c = 0;
    protected $users_c = 0;
    protected $new_users_c = 0;
    protected $log_buffer;
    protected $log;
    protected $timestamp;
    const EXACT    = 1;
    const INEXACT  = 2;
    const NOMATCH  = 3;
    const NAMEONLY = 4;

    public function __construct(MMBParserConfig $config)
    {
        $this->config = $config;
    }

    protected function loadMasterData()
    {
        $fd = fopen($this->config->source_folder.'/'.$this->config->master_users, 'r');

        while (!feof($fd) && ($str = trim(fgets($fd))))
        {
            $split = explode(",", $str);

            if (isset($split[0]) && isset($split[1]) && isset($split[2]))
            {
                $split[1] = str_replace("\"", "", $split[1]);
                $name_array = explode(" ", trim($split[1]));
                if (count($name_array) > 2)
                    $split[1] = $name_array[0].' '.$name_array[1];

                $user = [
                    'id'   => str_replace("\"", "", $split[0]),
                    'name' => $this->normalize(trim($split[1])),
                    'year' => (str_replace("\"", "", $split[2]) == 0) ? null : str_replace("\"", "", $split[2]),
                    'results' => [],
                    'teams' => []
                ];
            }

            $this->users[$user['id']] = $user;
        }

        fclose($fd);
        $fd = fopen($this->config->source_folder.'/'.$this->config->master_teams, 'r');

        while (!feof($fd) && ($str = trim(fgets($fd))))
        {
            $split = explode(",", $str);

            if (count($split) == 3)
            {
                $user_id = intval(str_replace("\"", "", $split[0]));
                $team_id = intval(str_replace("\"", "", $split[1]));
                $team_name = trim(str_replace("\"", "", $split[2]));

                $this->users[$user_id]['teams'][] = mb_strtolower($team_name, 'utf-8');
                sort($this->users[$user_id]['teams']);
                $this->users[$user_id]['teams'] = array_unique($this->users[$user_id]['teams']);
            }
        }
    }

    protected function slazav($file, $encoding)
    {
        $users = [];
        $fd = fopen($this->config->source_folder.'/'.$file, 'r');

        while (!feof($fd) && ($str = trim(fgets($fd)))) {
            $str = iconv($encoding, 'utf-8', $str);

            $split = explode("\t", $str);

            if (strpos($split[0], ","))
            {
                $nameyear = explode(",", $split[0]);
                $name = $nameyear[0];
                $year = $nameyear[1];
            } else {
                $name = $split[0];
                $year = null;
            }

            if (preg_match_all("/([0-9]+:[0-9]+)/u", $split[2], $match))
            {
                $result = $match[0][0];

                $result_split = explode(':', $result);
                $result = $result_split[0]*3600 + $result_split[1]*60;
            } else {
                $result = null;
            }

            if (preg_match_all("/([0-9]+) место/u", $split[2], $match))
            {
                $place = $match[1][0];
            } else {
                $place = $split[2];
            }

            if (preg_match_all("/N([0-9]+[ABАБВ]{0,1})/u", $split[1], $match))
            {
                $number = $match[1][0];
            } else {
                $number = null;
            }

            if (preg_match_all("/\"(.*)\"/u", $split[1], $match))
            {
                $team   = $match[1][0];
            } else {
                $team   = $number;
            }

            $user = [
                'name'   => $this->normalize(trim($name)),
                'number' => $number,
                'year'   => trim($year),
                'team'   => $team,
                'res'    => $result,
                'place'  => $place
            ];

            $users[] = $user;
        }

        return $users;
    }

    protected function csv2009fall($file, $encoding)
    {
        $users = [];
        $fd = fopen($this->config->source_folder.'/'.$file, 'r');
        $i = 0;
        while (!feof($fd) && ($str = trim(fgets($fd)))) {
            if ($i++ < 25) continue;
            $str = trim(iconv($encoding, 'utf-8', $str));

            $split = explode(";", $str);

            if (is_numeric($split[0]))
            {
                if (strpos($split[8], ", ") !== false)
                {
                    $res_split = explode(', ', $split[8]);
                    $res = explode(":", $res_split[1]);
                    $result = intval($res_split[0])*24*3600 + $res[0]*3600 + $res[1]*60 + $res[2];
                } elseif ($split[8] == 'None'){
                    $result = '';
                } else {
                    $res = explode(":", $split[8]);
                    if (count($res) == 3)
                    {
                        $result = $res[0]*3600 + $res[1]*60 + $res[2];
                    } else {
                        $result = $split[8];
                    }
                }

                $user = [
                    'name'   => '---',// $this->normalize(trim(str_replace('"', '', $name))),
                    'number' => $split[0],
                    'year'   => '---',
                    'team'   => trim($split[1]),
                    'res'    => $result,
                    'place'  => trim($split[9])
                ];

                //print_r($user);
            } else {
                $user_split = explode(";", $str);
                $user_name  = trim($user_split[1]);

                if (preg_match_all("/([0-9]{4})/u", $user_name, $match))
                {
                    $year = $match[0][0];
                } else {
                    $year = null;
                }

                if (preg_match_all("/([а-яА-Яa-zA-Z-]+){2,}/u", $user_name, $match))
                {
                    $user_name = $this->normalize(implode(' ', array_slice($match[0], 0, 2)));
                }

                $user2 = [
                    'name'   => $user_name,
                    'number' => $user['number'],
                    'year'   => $year,
                    'team'   => $user['team'],
                    'res'    => $user['res'],
                    'place'  => $user['place']
                ];

                $users[] = $user2;
            }

        }

        return $users;
    }

    protected function log($str, $status = false)
    {
        $this->log_buffer[] = $str;

        if ($status !== false)
        {
            $this->log[$status][] = implode("\n", $this->log_buffer);
            $this->log_buffer = [];
        }
    }

    protected function logReset()
    {
        file_put_contents($this->config->output_folder.'/'.$this->config->log, 'MMB PARSER LOG '.date("Y-m-d H:i:s")."\n\n");
    }

    protected function logSave()
    {
        $fd = fopen($this->config->output_folder.'/'.$this->config->log, 'a');

        for ($i = 1; $i <= 3; $i++)
        {
            if (isset($this->log[$i]) && is_array($this->log[$i]))
                fputs($fd, implode("\n", $this->log[$i])."\n\n");
            else
                fputs($fd, 'ERROR: LOG TYPE '.$i." NOT FOUND\n\n");
        }

        $this->log = [];

        fputs($fd, "----------------------------------\n\n");
        fclose($fd);
    }

    protected function csv2010($file, $encoding)
    {
        $users = [];
        $fd = fopen($this->config->source_folder.'/'.$file, 'r');
        $i = 0;
        while (!feof($fd) && ($str = trim(fgets($fd)))) {
            if ($i++ < 25) continue;
            $str = trim(iconv($encoding, 'utf-8', $str));

            $split = explode(";", $str);
            if (is_numeric($split[0]))
            {
                $res = explode(":", $split[10]);
                if (count($res) == 3)
                {
                    $result = $res[0]*3600 + $res[1]*60 + $res[2];
                } else {
                    $result = $split[10];
                }

                $user = [
                    'name'   => '---',// $this->normalize(trim(str_replace('"', '', $name))),
                    'number' => $split[0],
                    'year'   => '---',
                    'team'   => trim($split[1]),
                    'res'    => $result,
                    'place'  => trim($split[11])
                ];

                //print_r($user);
            } else {
                $user_split = explode(";", $str);
                $user_name  = trim($user_split[1]);

                if (preg_match_all("/([0-9]{4})/u", $user_name, $match))
                {
                    $year = $match[0][0];
                } else {
                    $year = null;
                }

                if (preg_match_all("/([а-яА-Яa-zA-Z-]+)/u", $user_name, $match))
                {
                    $user_name = $this->normalize(implode(' ', $match[0]));
                }

                $user2 = [
                    'name'   => $user_name,
                    'number' => $user['number'],
                    'year'   => $year,
                    'team'   => $user['team'],
                    'res'    => $user['res'],
                    'place'  => $user['place']
                ];

                $users[] = $user2;
            }

        }

        return $users;
    }

    protected function mysqlImport($raid_id, $enc = 'utf8')
    {
        $users = [];

        $db = mysqli_connect($this->config->db->host, $this->config->db->user, $this->config->db->password, $this->config->db->dbname);
        mysqli_query($db, 'set names utf8');

        $query = '
            SELECT
                team_num, t.distance_id, user_name, user_birthyear, team_result, team_name
            FROM
                Users u, Teams t, TeamUsers tu, Distances d
            WHERE
                d.raid_id = '.$raid_id.' AND d.distance_id = t.distance_id AND tu.team_id = t.team_id AND tu.user_id = u.user_id';

        $res = mysqli_query($db, $query);

        while ($row = mysqli_fetch_row($res))
        {
            if (!is_null($row[4]))
            {
                $res_split = explode(':', $row[4]);
                $result = $res_split[0] * 3600 + $res_split[1] * 60 + $res_split[2];
            } else {
                $result = null;
            }

            $users[] = [
                'name'   => $this->normalize(implode(' ', array_slice(explode(" ", trim($row[2])), 0, 2))),
                'number' => $row[0],
                'year'   => $row[3],
                'team'   => $row[5],
                'res'    => $result,
                'place'  => ''
            ];

            //print_r($row);
        }

        return $users;
        //exit;
    }

    protected function csv2010fall($file, $encoding)
    {
        $users = [];

        $full_html = file_get_contents($this->config->source_folder.'/'.$file);

        preg_match_all('/<tr>(.*)<\/tr>/U', $full_html, $matches);

        foreach ($matches[1] as $tr)
        {
            preg_match_all('/<td.*>(.*)<\/td.*>/U', $tr, $tds);
            if (count($tds[1]) == 11)
            {
                preg_match('/^([0-9]+)/u', $tds[1][0], $number);
                preg_match('/^<b>(.*)<\/b>/u', $tds[1][1], $name);
                $number = intval($number[0]);
                $team_name = strval($name[1]);
                $result_explode = explode(':', trim($tds[1][9]));
                if (count($result_explode) == 2)
                {
                    $time = $result_explode[0] * 3600 + $result_explode[1] * 60;
                } else {
                    $time = null;
                }

                preg_match('/^<b>(.*)<\/b>/u', $tds[1][10], $place);

                if (is_numeric($place[1]))
                {
                    $place = $place[1];
                } else {
                    $place = null;
                }

                $explode = explode("<br>", $tds[1][1]);
                unset($explode[count($explode) - 1]);
                unset($explode[0]);

  //              print_r($tds[1]);

                foreach ($explode as $user_string)
                {
                    if (preg_match('/([0-9]{4})/u', $user_string, $year))
                    {
                        $year = $year[1];
                    } else {
                        $year = null;
                    }

                    $name = implode(' ', array_slice(explode(" ", trim($user_string)), 0, 2));
//                    print $team_name."\t".$name." : ".$year."\n";

                    $user = [
                        'name'   => $this->normalize(trim($name)),
                        'number' => $number,
                        'year'   => $year,
                        'team'   => $team_name,
                        'res'    => $time,
                        'place'  => $place
                    ];

                    $users[] = $user;
                }


            }
        }

        return $users;
    }


    protected function csv2011($file, $encoding)
    {
        $users = [];
        $fd = fopen($this->config->source_folder.'/'.$file, 'r');
        $i = 0;
        while (!feof($fd) && ($str = trim(fgets($fd)))) {
            if ($i++ < 13) continue;
            $str = trim(iconv($encoding, 'utf-8', $str));

            $split = explode(";", $str);
            if (is_numeric($split[0]))
            {
                $res = explode(":", $split[8]);
                if (count($res) == 2)
                {
                    $result = $res[0]*3600 + $res[1]*60;
                } elseif (count($res) == 3)
                {
                    $result = $res[0]*3600 + $res[1]*60 + $res[2];
                } else {
                    $result = '';//$split[8];
                }

                $user = [
                    'name'   => '---',// $this->normalize(trim(str_replace('"', '', $name))),
                    'number' => $split[0],
                    'year'   => '---',
                    'team'   => trim($split[1]),
                    'res'    => $result,
                    'place'  => trim($split[9])
                ];

            } else {
                $user_split = explode(";", $str);
                $user_name  = trim($user_split[1]);

                if (preg_match_all("/([0-9]{4})/u", $user_name, $match))
                {
                    $year = $match[0][0];
                } else {
                    $year = null;
                }

                if (preg_match_all("/([а-яА-Яa-zA-Z-]+)/u", $user_name, $match))
                {
                    $user_name = $this->normalize(implode(' ', $match[0]));
                }

                $user2 = [
                    'name'   => $user_name,
                    'number' => $user['number'],
                    'year'   => $year,
                    'team'   => $user['team'],
                    'res'    => $user['res'],
                    'place'  => $user['place']
                ];

                if (mb_strlen($user2['name']) > 0)
                    $users[] = $user2;
            }
        }

        return $users;
    }

    protected function csv2011fall($file, $encoding)
    {
        $users = [];
        $fd = fopen($this->config->source_folder.'/'.$file, 'r');
        $i = 0;
        while (!feof($fd) && ($str = trim(fgets($fd)))) {
            if ($i++ < 9) continue;
            $str = trim(iconv($encoding, 'utf-8', $str));

            $split = explode(";", $str);
            if (is_numeric($split[0]))
            {
                $res = explode(":", $split[8]);
                if (count($res) == 2)
                {
                    $result = $res[0]*3600 + $res[1]*60;
                } elseif (count($res) == 3)
                {
                    $result = $res[0]*3600 + $res[1]*60 + $res[2];
                } else {
                    $result = $split[8];
                }

                $user = [
                    'name'   => '---',// $this->normalize(trim(str_replace('"', '', $name))),
                    'number' => $split[0],
                    'year'   => '---',
                    'team'   => trim($split[1]),
                    'res'    => $result,
                    'place'  => trim($split[9])
                ];

            } else {
                $user_split = explode(";", $str);
                $user_name  = trim($user_split[1]);

                if (preg_match_all("/([0-9]{4})/u", $user_name, $match))
                {
                    $year = $match[0][0];
                } else {
                    $year = null;
                }

                if (preg_match_all("/([а-яА-Яa-zA-Z-]+)/u", $user_name, $match))
                {
                    $user_name = $this->normalize(implode(' ', $match[0]));
                }

                $user2 = [
                    'name'   => $user_name,
                    'number' => $user['number'],
                    'year'   => $year,
                    'team'   => $user['team'],
                    'res'    => $user['res'],
                    'place'  => $user['place']
                ];

                if ((mb_strlen($user2['name']) > 0) && ($user2['name'] != 'сход участника'))
                    $users[] = $user2;
            }
        }

        return $users;
    }


    protected function csv2009($file, $encoding)
    {
        $users = [];
        $fd = fopen($this->config->source_folder.'/'.$file, 'r');

        while (!feof($fd) && ($str = trim(fgets($fd)))) {
            $str = trim(iconv($encoding, 'utf-8', $str));
            $newstr = '';
            $quotes_cnt = 0;
            for ($i = 0; $i < mb_strlen($str); $i++)
            {
                $char = mb_substr($str, $i, 1);
                if ($char == '"')
                    $quotes_cnt++;

                if (($quotes_cnt % 2 == 1) && ($char == ','))
                    $newstr .= '#';
                else
                    $newstr .= $char;
            }

            $str = $newstr;

            $split = explode(",", $str);

            $split_users = explode(";", str_replace('"', '', $split[5]));
            foreach ($split_users as $user_str)
            {
                $split_year = explode("#", $user_str);

                if (count($split_year) == 2)
                {
                    $name = $split_year[0];
                    $year = $split_year[1];
                } else {
                    $name = $user_str;
                    $year = null;
                }

                $user = [
                    'name'   => $this->normalize(trim(str_replace('"', '', $name))),
                    'number' => str_replace('"', '', $split[0]),
                    'year'   => trim($year),
                    'team'   => str_replace('#', ',', trim(str_replace('"', '', $split[3]))),
                    'res'    => intval(trim(str_replace('"', '', $split[21])))*60,
                    'place'  => trim(str_replace('"', '', $split[22]))
                ];


                $users[] = $user;
            }
        }

        return $users;
    }

    protected function merge($id, $users)
    {
    //    $this->log("ROUTE ".$id." from ".$this->config->data[$id]['file']." (".$this->config->data[$id]['name'].")" );

        $leader_time = false;
        foreach ($users as $user)
        {
            if (($leader_time == false) && (intval($user['res']) > 0))
            {
                $leader_time = intval($user['res']);
            }

            if (intval($user['place']) == 1)
            {
                $leader_time = intval($user['res']);
                break;
            }

            if ((intval($user['res']) > 0) && (intval($user['res']) < $leader_time))
            {
                $leader_time = intval($user['res']);
            }
        }

     //   $this->log('calculated winner result = '.$leader_time);

        $users_c = $matched_c = $new_users_c = 0;
        foreach ($users as $user)
        {
            $this->log('match lookup: name="'.$user['name'].'", year="'.$user['year'].
                '", number="'.$user['number'].'", team="'.$user['team'].'", result="'.$user['res'].'", place="'.$user['place'].'"');

            $nearest = $this->searchNearestUser($user);

            if ($nearest !== false)
            {
                //    printf ("%4d %s (%s) \t\t%4d %s #%d (%s)\n",
                //        $user['year'],  $user['name'], $user['team'],
                //        $nearest['year'], $nearest['name'], $nearest['id'], implode(', ', $nearest['teams']));
                $matched_c++;
                $this->matched_c++;
                print "+";

            } else {
                //    printf ("%4d %s (%s) \t\tNO MATCH\n",
                //        $user['year'],  $user['name'], $user['team']);
                print ".";

                $this->users[] = [
                    'name' => $user['name'],
                    'year' => $user['year'],
                    'results' => [],
                    'teams' => [$user['team']]
                ];

                end($this->users);         // move the internal pointer to the end of the array
                $key = key($this->users);
                $this->users[$key]['id'] = $key;
                $nearest = $this->users[$key];

                $new_users_c++;
                $this->new_users_c++;
            }

            $users_c++;
            $this->users_c++;

            $this->users[$nearest['id']]['results'][$id] = [
                'time'  => $user['res'],
                'place' => $user['place'],
                'leader'=> $leader_time,
                'team'  => $user['team'],
                'number'=> $user['number']
            ];

            $this->teams[$id][$user['number']]['team'] = $user['team'];
            $this->teams[$id][$user['number']]['time'] = $user['res'];

            $this->users[$nearest['id']]['teams'][] = mb_strtolower($user['team'], 'utf-8');
            sort($this->users[$nearest['id']]['teams']);
            $this->users[$nearest['id']]['teams'] = array_unique($this->users[$nearest['id']]['teams']);

        }

        print "\n".$matched_c."/".$users_c." (".number_format($matched_c/$users_c*100, 1)."%) matched, ".$new_users_c." added \n";
    }

    protected function normalize($str)
    {
        return str_replace('ё', 'е', mb_strtolower($str, 'utf-8'));
    }

    protected function logTransform($user)
    {
        return 'id='.$user['id'].', name='.$user['name'].', year='.$user['year'].', teams='.implode('; ',$user['teams']);

    }

    protected function searchNearestUser($user)
    {
        $matches = [];
        $name_only = [];

        foreach ($this->users as $master_user)
        {
            $match_score = $this->matchUser($user, $master_user);
            if ($match_score == self::EXACT)
            {
                $this->log('  using exact match with '.$this->logTransform($master_user), self::EXACT);
                return $master_user;
            }

            if ($match_score == self::INEXACT)
            {
                $matches[] = $master_user;
            }

            if ($match_score == self::NAMEONLY)
            {
                $name_only[] = $master_user;
            }
        }

        $min_dist = 1000;
        $min_dist_id = false;

        if (count($matches) > 0){
            foreach ($matches as $match)
            {
                $this->log('    option '.$this->logTransform($match));
                if (is_array($match['teams']))
                {
                    foreach ($match['teams'] as $team)
                    {
                        $dist = levenshtein(mb_strtolower($team, 'utf-8'), mb_strtolower($user['team'], 'utf-8'));

                        $this->log('      dist='.$dist."\t".'team1='.$user['team'].', team2='.$team);

                        if ($dist < $min_dist){
                            $min_dist = $dist;
                            $min_dist_id = $match['id'];
                        }
                    }
                }
            }



            if (($min_dist_id !== false) && ($min_dist <= 5))
            {
                $this->log('  using nearest (dist='.$min_dist.') '.$this->logTransform($this->users[$min_dist_id]), self::INEXACT);
                return $this->users[$min_dist_id];
            } elseif  (($min_dist_id !== false) && ($min_dist > 5)) {
                foreach ($matches as $match)
                {

                    if ($this->splitSort($match['name']) == $this->splitSort($user['name']))
                    {
                        $this->log('  teams too far, using name-only match '.$this->logTransform($match), self::INEXACT);
                        return $match;
                    }

                }

                $this->log('  matches not found, teams too far', self::NOMATCH);
                return false;

            } else {
                // case without teams
                $this->log('  using nearest '.$this->logTransform($matches[0]), self::INEXACT);
                return $matches[0];
            }
        } else {
            if (count($name_only) > 0)
            {
                $this->log('  using name-only match (nearest not found) '.$this->logTransform($name_only[0]), self::INEXACT);
                return $name_only[0];
            } else {
                $this->log('  matches not found ', self::NOMATCH);
                return false;
            }
        }
    }

    protected function splitSort($str, $glue = '', $limit = 2)
    {
        $arr = explode(" ", $str);
        $arr = array_slice($arr, 0, $limit);
        sort($arr);
        return implode($glue, $arr);
    }

    protected function matchUser($user1, $user2)
    {
        $name1 = $this->splitSort($user1['name']);
        $name2 = $this->splitSort($user2['name']);

        $levenshtein = levenshtein($name1, $name2);
        $year_exist = (intval($user1['year']) > 1900) && (intval($user2['year']) > 1900);

        if (($name1 == $name2) &&  $year_exist && $user1['year'] == $user2['year'])
            return self::EXACT;

        if (($levenshtein < 3) &&  $year_exist && $user1['year'] == $user2['year'])
            return self::INEXACT;

        if (($levenshtein < 3) &&  !$year_exist)
            return self::INEXACT;

        if ($levenshtein == 0)
            return self::NAMEONLY;

        return self::NOMATCH;
    }

    protected function scoreUsers()
    {
        foreach ($this->users as $id => $user)
        {
            $score = 0;
            foreach ($user['results'] as $result)
            {
                if (intval($result['time']) > 0)
                {
                    $score += intval(pow ( $result['leader'] / $result['time'], 3) * 1000);
                }
            }

            $this->users[$id]['score'] = $score;
        }
    }

    protected function saveTeamsCSV()
    {
        $csv = '';
        foreach ($this->teams as $raid_id => $raid_teams)
        {
            foreach ($raid_teams as $number => $team)
            {
                $csv .= $team['id'].';'.$this->config->data[$raid_id]['raid_id'].';'.$number.';'.$team['team']."\n";
            }
        }

        file_put_contents(dirname(__FILE__).'/../output/teams.csv', $csv);
    }

    public static function timestamp2time($ts)
    {
        if (!is_numeric($ts))
            return '00:00:00'; // team not finished value

        if ($ts > 0)
        {
            $h = floor($ts / 3600);
            $m = floor(($ts - 3600*$h) / 60);
            $s = $ts - 3600*$h - 60*$m;

            return sprintf("%d:%02d:%02d", $h, $m, $s);
        } else {
            return '00:00:00'; // team not finished value
        }
    }

    protected function saveTeamsSQL()
    {
        $sql = '';
        foreach ($this->teams as $raid_id => $raid_teams)
        {
            foreach ($raid_teams as $number => $team)
            {
                $sql .= sprintf('INSERT INTO Teams (team_importattempt, team_id, team_name, distance_id, team_num, team_usegps, team_mapscount, team_registerdt, team_confirmresult, team_greenpeace, team_moderatorconfirmresult, team_parentid, team_result, team_hide) VALUES (%d, %d, "%s", %d, %d, %d, %d, "%s", %d, %d, %d, %d, "%s", 0);',
                    $this->timestamp, $team['id'], str_replace('"', '\"', $team['team']), $raid_id, intval($number), 0, 0, '2014-05-28', 1, 1, 1, 0, self::timestamp2time($team['time']))."\n";

            }
        }

        file_put_contents(dirname(__FILE__).'/../output/teams.sql', $sql);
    }

    protected function saveUsersCSV()
    {
        $csv = '';
        foreach ($this->users as $id => $user)
        {
            $csv .= $id.';'.mb_convert_case($user['name'], MB_CASE_TITLE, 'utf-8').';'.$user['year']."\n";
        }

        file_put_contents(dirname(__FILE__).'/../output/users.csv', $csv);
    }

    protected function saveUsersSQL()
    {
        $sql = '';
        foreach ($this->users as $id => $user)
        {
            if ($id > 4466)
            $sql .= sprintf('INSERT INTO Users (user_importattempt, user_id, user_name, user_email, user_password, user_birthyear, user_registerdt) VALUES (%d, %d, "%s", "auto%s@mmb.ru", "asd123", "%d", "2014-05-28");',
                    $this->timestamp, $id, mb_convert_case($user['name'], MB_CASE_TITLE, 'utf-8'), $id, $user['year'])."\n";
        }

        file_put_contents(dirname(__FILE__).'/../output/users.sql', $sql);
    }

    protected function saveUserTeamsCSV()
    {
        $csv = '';
        foreach ($this->users as $id => $user)
        {
            foreach ($user['results'] as $raid_id => $result)
            {
                $csv .= $id.';'.$this->config->data[$raid_id]['raid_id'].';'.$this->teams[$raid_id][$result['number']]['id']."\n";
            }
        }

        file_put_contents(dirname(__FILE__).'/../output/user_teams.csv', $csv);
    }

    protected function saveUserTeamsSQL()
    {
        $sql = '';
        foreach ($this->users as $id => $user)
        {
            foreach ($user['results'] as $raid_id => $result)
            {
                $sql .= sprintf( 'INSERT INTO TeamUsers (teamuser_importattempt, team_id, user_id, teamuser_hide) VALUES (%d, %d, %d, 0);',
                    $this->timestamp, $this->teams[$raid_id][$result['number']]['id'], $id)."\n";
            }
        }

        file_put_contents(dirname(__FILE__).'/../output/user_teams.sql', $sql);
    }

    protected function saveFullCSV()
    {
        $csv = '';
        foreach ($this->users as $id => $user)
        {
            foreach ($user['results'] as $raid_id => $result)
            {
                if (!isset($this->teams[$raid_id][$result['number']]['id']))
                {
                    print "cant find team raid_id=".$raid_id.", number=".$result['number']."\n";
                }
                $csv .= implode(';', [
                    $this->config->data[$raid_id]['raid_id'],
                    $id,
                    $user['name'],
                    $user['year'],
                    $this->teams[$raid_id][$result['number']]['id'],
                    $result['time'],
                    $result['place'],
                    $result['leader'],
                    $result['team'],
                    $result['number'],
                ])."\n";
            }
        }

        file_put_contents(dirname(__FILE__).'/../output/full.csv', $csv);
    }

    protected function generateTeamIds()
    {
        $i = 10000;
        foreach ($this->teams as $raid_id => $raid_teams)
        {
            foreach ($raid_teams as $number => $team)
            {
                $this->teams[$raid_id][$number]['id'] = $i++;
            }
        }
    }

    public function run()
    {
        $this->timestamp = time();
        file_put_contents($this->config->output_folder.'/'.$this->config->log, '');
        $this->loadMasterData();
        $this->logReset();

        foreach ($this->config->data as $id => $source)
        {
            print "\n=== ".$source['name']." ===\n";
            $results = $this->{$source['parser']}($source['file'], $source['encoding']);
            $this->merge($id, $results);
            $this->logSave();
        }

        print "OVERALL REPORT: ".$this->matched_c."/".$this->users_c." (".number_format($this->matched_c/$this->users_c*100, 1)."%) matched, ".$this->new_users_c." added\n";
        print "saving output...";
        $this->scoreUsers();
        $this->generateTeamIds();
        $this->saveTeamsCSV();
        $this->saveUsersCSV();
        $this->saveUserTeamsCSV();
        $this->saveFullCSV();

        $this->saveTeamsSQL();
        $this->saveUsersSQL();
        $this->saveUserTeamsSQL();

        file_put_contents(dirname(__FILE__).'/../output/users.json', json_encode($this->users));
        file_put_contents(dirname(__FILE__).'/../output/teams.json', json_encode($this->teams));
        print "DONE\n\n";
    }
}