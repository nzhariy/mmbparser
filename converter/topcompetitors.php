<?php

require dirname(__FILE__).'/config.php';

$users = json_decode(file_get_contents(dirname(__FILE__).'/../output/users.json'), true);

foreach ($users as $id => $user)
{
    $completed = 0;
    $started = 0;

    foreach ($user['results'] as $result)
    {
        if (intval($result['time']) > 0)
        {
            $completed++;
        }

        $started++;
    }

    $users[$id]['completed'] = $completed;
    $users[$id]['started'] = $started;
}

$users_canonic = $users;

usort($users, function ($a, $b) {

    if ($a['started'] - $a['completed'] == $b['started'] - $b['completed'])
        return 0;

    if ($a['started'] - $a['completed'] < $b['started'] - $b['completed'])
        return 1;

    if ($a['started'] - $a['completed'] > $b['started'] - $b['completed'])
        return -1;
});

$users = array_slice($users, 0, 30);

print "\n\nТОП упорства\n\n Место    Фамилия, Имя        ";

    printf("%-6s    %-6s %-6s  %-6s %-6s \n", 'год', 'дошел', 'старт', 'очки', 'неуд.попыток');
$i = 0;

foreach ($users as $user)
{
    printf ("%3d. %s%s%4s%s", 
		++$i, 
		mb_convert_case($user['name'], MB_CASE_TITLE, 'utf-8'), 
		str_repeat('.', 25 - mb_strlen($user['name'], 'utf-8')), 
		intval($user['year'])>0 ? $user['year'] : '....', 
		str_repeat(' ', 5));

    printf("%2d  %4d", $user['completed'], $user['started']);
    printf("  %5d %6d\n", $user['score'], $user['started'] - $user['completed']);
}

$users = $users_canonic;

usort($users, function ($a, $b) {

    if ($a['started'] == $b['started'])
        return 0;

    if ($a['started'] < $b['started'])
        return 1;

    if ($a['started'] > $b['started'])
        return -1;
});

$users = array_slice($users, 0, 30);

print "\n\nТОП кол-ва попыток\n\n Место    Фамилия, Имя        ";

    printf("%-6s     %-6s %-6s   %-6s \n", 'год', 'дошел', 'старт', 'очки');
$i = 0;

foreach ($users as $user)
{
    printf ("%3d. %s%s%4s%s", 
		++$i, 
		mb_convert_case($user['name'], MB_CASE_TITLE, 'utf-8'), 
		str_repeat('.', 25 - mb_strlen($user['name'], 'utf-8')), 
		intval($user['year'])>0 ? $user['year'] : '....', 
		str_repeat(' ', 5));

    printf(" %2d  %4d", $user['completed'], $user['started']);
    printf("   %5d\n", $user['score']);
}


$users = $users_canonic;

usort($users, function ($a, $b) {

    if ($a['completed'] == $b['completed'])
        return 0;

    if ($a['completed'] < $b['completed'])
        return 1;

    if ($a['completed'] > $b['completed'])
        return -1;
});

$users = array_slice($users, 0, 30);

print "\n\nТОП кол-ва финишей\n\n Место    Фамилия, Имя        ";

printf("%-6s     %-6s %-6s   %-6s \n", 'год', 'дошел', 'старт', 'очки');
$i = 0;

foreach ($users as $user)
{
    printf ("%3d. %s%s%4s%s", 
		++$i, 
		mb_convert_case($user['name'], MB_CASE_TITLE, 'utf-8'), 
		str_repeat('.', 25 - mb_strlen($user['name'], 'utf-8')), 
		intval($user['year'])>0 ? $user['year'] : '....', 
		str_repeat(' ', 5));

    printf(" %2d  %4d", $user['completed'], $user['started']);
    printf("   %5d\n", $user['score']);
}

