<?php

date_default_timezone_set('Europe/Moscow');

class MMBDbConfig {
    public $user;
    public $password;
    public $dbname;
    public $host;
}

class MMBParserConfig {
    public $source_folder;
    public $output_folder;
    public $master_users;
    public $master_teams;
    public $data;
    public $log;
    /**
     * @var MMBDbConfig
     */
    public $db;
}

$config = new MMBParserConfig();
$config->source_folder = dirname(__FILE__).'/../source';
$config->output_folder = dirname(__FILE__).'/../output';
$config->master_users = 'master/Users_2014v.csv';
$config->master_teams = 'master/TeamUsers_2014v.csv';
$config->db = new MMBDbConfig();
$config->db->user = 'root';
$config->db->password = 'aerosmith';
$config->db->dbname = 'mmb';
$config->db->host = '127.0.0.1';
$config->log = 'matching_log.txt';
$config->data = [
    30 => ['file' => 23,                      'raid_id' => 23, 'key' => '2014в', 'encoding' => '', 'name' => '2014 весна',   'parser' => 'mysqlImport'],
    29 => ['file' => 22,                      'raid_id' => 22, 'key' => '2013о', 'encoding' => '', 'name' => '2013 осень',   'parser' => 'mysqlImport'],
    28 => ['file' => 21,                      'raid_id' => 21, 'key' => '2013в', 'encoding' => '', 'name' => '2013 весна',   'parser' => 'mysqlImport'],
    27 => ['file' => 20,                      'raid_id' => 20, 'key' => '2012о', 'encoding' => '', 'name' => '2012 осень',   'parser' => 'mysqlImport'],
    8  => ['file' => 19,                      'raid_id' => 19, 'key' => '2012в', 'encoding' => '', 'name' => '2012 весна',   'parser' => 'mysqlImport'],
    26 => ['file' => '2011f/2011o-win.csv',   'raid_id' => 18, 'key' => '2011о', 'encoding' => 'cp1251', 'name' => '2011 осень',   'parser' => 'csv2011fall'],
    25 => ['file' => '2011/2011v-win.csv',    'raid_id' => 17, 'key' => '2011в', 'encoding' => 'cp1251', 'name' => '2011 весна',   'parser' => 'csv2011'],
    24 => ['file' => '2010f/results10o.html', 'raid_id' => 16, 'key' => '2010о', 'encoding' => 'utf-8',  'name' => '2010 осень',   'parser' => 'csv2010fall'],
    23 => ['file' => '2010/2010v-win.csv',    'raid_id' => 15, 'key' => '2010в', 'encoding' => 'cp1251', 'name' => '2010 весна',   'parser' => 'csv2010'],
    22 => ['file' => '2009f/2009o-win.csv',   'raid_id' => 14, 'key' => '2009о', 'encoding' => 'cp1251', 'name' => '2009 осень',   'parser' => 'csv2009fall'],
    20 => ['file' => '2009/res09v-a-win.csv', 'raid_id' => 13, 'key' => '2009в', 'encoding' => 'cp1251', 'name' => '2009 весна A', 'parser' => 'csv2009'],
    21 => ['file' => '2009/res09v-b-win.csv', 'raid_id' => 13, 'key' => '2009в', 'encoding' => 'cp1251', 'name' => '2009 весна B', 'parser' => 'csv2009'],
    18 => ['file' => '2008f/ludi08oa.txt',    'raid_id' => 12, 'key' => '2008о', 'encoding' => 'koi8-r', 'name' => '2008 осень A', 'parser' => 'slazav'],
    19 => ['file' => '2008f/ludi08ob.txt',    'raid_id' => 12, 'key' => '2008о', 'encoding' => 'koi8-r', 'name' => '2008 осень B', 'parser' => 'slazav'],
    16 => ['file' => '2008/ludi08a.txt',      'raid_id' => 11, 'key' => '2008в', 'encoding' => 'koi8-r', 'name' => '2008 весна А', 'parser' => 'slazav'],
    17 => ['file' => '2008/ludi08b.txt',      'raid_id' => 11, 'key' => '2008в', 'encoding' => 'koi8-r', 'name' => '2008 весна B', 'parser' => 'slazav'],
    15 => ['file' => '2007f/ludi07o.txt',     'raid_id' => 10, 'key' => '2007о', 'encoding' => 'koi8-r', 'name' => '2007 осень',   'parser' => 'slazav'],
    13 => ['file' => '2007/ludi07a.txt',      'raid_id' =>  9, 'key' => '2007в', 'encoding' => 'koi8-r', 'name' => '2007 весна А', 'parser' => 'slazav'],
    14 => ['file' => '2007/ludi07b.txt',      'raid_id' =>  9, 'key' => '2007в', 'encoding' => 'koi8-r', 'name' => '2007 весна B', 'parser' => 'slazav'],
    11 => ['file' => '2006f/ludi06oa.txt',    'raid_id' =>  8, 'key' => '2006о', 'encoding' => 'koi8-r', 'name' => '2006 осень A', 'parser' => 'slazav'],
    12 => ['file' => '2006f/ludi06ob.txt',    'raid_id' =>  8, 'key' => '2006о', 'encoding' => 'koi8-r', 'name' => '2006 осень B', 'parser' => 'slazav'],
     9 => ['file' => '2006/ludi06a.txt',      'raid_id' =>  7, 'key' => '2006в', 'encoding' => 'koi8-r', 'name' => '2006 весна А', 'parser' => 'slazav'],
    10 => ['file' => '2006/ludi06b.txt',      'raid_id' =>  7, 'key' => '2006в', 'encoding' => 'koi8-r', 'name' => '2006 весна B', 'parser' => 'slazav'],
     7 => ['file' => '2005f/ludi05lb.txt',    'raid_id' =>  6, 'key' => '2005о', 'encoding' => 'koi8-r', 'name' => '2005 осень B', 'parser' => 'slazav'],
     6 => ['file' => '2005f/ludi05la.txt',    'raid_id' =>  6, 'key' => '2005о', 'encoding' => 'koi8-r', 'name' => '2005 осень А', 'parser' => 'slazav'],
     5 => ['file' => '2005/ludi05.txt',       'raid_id' =>  5, 'key' => '2005в', 'encoding' => 'koi8-r', 'name' => '2005 весна',   'parser' => 'slazav'],
     4 => ['file' => '2004f/ludi04l.txt',     'raid_id' =>  4, 'key' => '2004о', 'encoding' => 'koi8-r', 'name' => '2004 осень',   'parser' => 'slazav'],
     3 => ['file' => '2004/ludi04.txt',       'raid_id' =>  3, 'key' => '2004в', 'encoding' => 'koi8-r', 'name' => '2004 весна',   'parser' => 'slazav'],
     2 => ['file' => '2003f/ludi03l.txt',     'raid_id' =>  2, 'key' => '2003о', 'encoding' => 'koi8-r', 'name' => '2003 осень',   'parser' => 'slazav'],
     1 => ['file' => '2003/ludi03.txt',       'raid_id' =>  1, 'key' => '2003в', 'encoding' => 'koi8-r', 'name' => '2003 весна',   'parser' => 'slazav'],
];
