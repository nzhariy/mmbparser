<?php

require dirname(__FILE__).'/config.php';

$users = json_decode(file_get_contents(dirname(__FILE__).'/../output/users.json'), true);


usort($users, function ($a, $b) {

   if ($a['score'] == $b['score'])
       return 0;

   if ($a['score'] < $b['score'])
       return 1;

   if ($a['score'] > $b['score'])
       return -1;
});

$users = array_slice($users, 0, 100);

print " Место    Фамилия, Имя        ";

foreach ($config->data as $id => $competition)
{
    printf("%-6s ", $competition['key']);
}
$i = 0;
print "СУММА\n";

foreach ($users as $user)
{
    printf ("%3d. %s%s", ++$i, mb_convert_case($user['name'], MB_CASE_TITLE, 'utf-8'), str_repeat('.', 25 - mb_strlen($user['name'], 'utf-8')));

    foreach ($config->data as $id => $competition)
    {
        printf("%-6d", (isset($user['results'][$id]) && (intval($user['results'][$id]['time'])> 0)) ? intval(
            1000*pow($user['results'][$id]['leader'] / $user['results'][$id]['time'] , 3)) : '');
        //printf('%-6d', isset($user['results'][$id])?$user['results'][$id]['time']:'');
    }

    printf("%-6d\n", $user['score']);
}