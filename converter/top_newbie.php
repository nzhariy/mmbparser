<?php

require dirname(__FILE__).'/config.php';

$users = json_decode(file_get_contents(dirname(__FILE__).'/../output/users.json'), true);

$raids_info = [];

foreach ($config->data as $distance_id => $raid)
{
    $raids_info[$raid['raid_id']] = $raid['key'];
}


$raids_all = $raids_finished = [];

foreach ($users as $id => $user)
{
    foreach ($user['results'] as $distance_id => $result)
    {
        $raids_all[$config->data[$distance_id]['raid_id']][] = $id;

        if (intval($result['time']) > 0)
        {
            $raids_finished[$config->data[$distance_id]['raid_id']][] = $id;
        }
    }
}

ksort($raids_all);

print "              ВСЕГО ФИНИШ        НОВЫХ  НОВ.ФИН.  %Ф/Н  %Н/ВСЕ  %НФ/ВСЕ  %РЕЦ/НФ %РЕЦ/НОВ.\n";

for ($i = 1; $i <= 23; $i++)
{
    $this_raid_users = $raids_all[$i];
    $prev_raid_users = [];

    for ($j = $i - 1; $j > 0; $j--)
    {
        $prev_raid_users = array_unique(array_merge($prev_raid_users, $raids_all[$j]));
    }

    $intersect = array_intersect($this_raid_users, $prev_raid_users);
    $newbies   = array_diff($this_raid_users, $intersect);

    $recurrenced = $recurrenced_finished = [];
    foreach ($newbies as $newbie_id)
    {
        if (count($users[$newbie_id]['results']) > 1)
        {
            $recurrenced[] = $newbie_id;
        }
    }

    $newbies_finished = array_intersect($newbies, $raids_finished[$i]);

    foreach ($newbies_finished as $newbie_id)
    {
        if (count($users[$newbie_id]['results']) > 1)
        {
            $recurrenced_finished[] = $newbie_id;
        }
    }

    //print intval((count($this_raid_users) - count($intersect))/count($this_raid_users)*100)."% OLD=".count($intersect)." NEW=".(count($this_raid_users) - count($intersect))."\n";
    printf("%10s     %4d  %4d  %4d%%  %4d   %4d    %4d%%   %4d%%    %4d%%   %4d%%   %4d%%\n",
        $raids_info[$i],                         // raid_id
        count($raids_all[$i]),      // total_users
        count($raids_finished[$i]), // total_users_finished
        count($raids_finished[$i])/count($raids_all[$i])*100, // total_users_finished %
        count($newbies),          // newbies
        count($newbies_finished),           // newbies finished
        count($newbies_finished)/count($newbies)*100,           // newbies finished %
        count($newbies)/count($this_raid_users)*100,           // newbies %
        count($newbies_finished)/count($raids_all[$i])*100,        // newbies finished % / total users
        count($recurrenced_finished)/count($newbies_finished)*100,        //  % recurrenced / newbies_finished
        count($recurrenced)/count($newbies)*100        //  % recurrenced / newbies_total
    );
}

print "\n\n";
print "ВСЕГО    – общее количество участников, вышедших на старт\n";
print "ФИНИШ    – общее количество финишировавших участников\n";
print "НОВЫХ    – количество участников, которые до этого не участвовали ни в одном ММБ\n";
print "НОВ.ФИН. – количество участников, которые до этого не участвовали ни в одном ММБ и успешно финишировали\n";
print "%Н/ВСЕ – процент участников, которые попали на ММБ впервые к общему числу участников этапа\n";
print "%НФ/ВСЕ  – процент участников, которые попали на ММБ впервые и финишировали к общему числу участников этапа\n";
print "%РЕЦ/НФ  – процент новичков, которые пошли на ММБ повторно к числу финишировавших новичков\n";
print "%РЕЦ/НОВ.– процент новичков, которые пошли на ММБ повторно к общему числу новичков\n\n";
