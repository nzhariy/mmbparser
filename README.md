# README #

Конвертор старых результатов ММБ в новый формат с метчингом

### How to run ###

```
#!bash

mkdir project
cd project
git clone git@bitbucket.org:nzhariy/mmbparser.git .
php converter/convert.php
```

### Contents ###
```
* converter                       - scripts
* source                          - source data
* output                          - output data
* converter/convert.php           - convert source/* -> output/users.json
* converter/top100.php            - make top100 from output/users.json 
* converter/config.php            - config file 
* converter/parser.php            - parser class
* converter/reader.php [filename] - reads from users.json in pretty readable format
* source/XXXX[f]                  - mmb results by YEAR[Fall]
* source/master                   - mmb master data
* output/users.json               - aggregated users
* output/teams.json               - aggregated teams
* output/*.sql                    - SQL inserts (users, teams, user-teams)
```

### Top-100 ###

Score = SUM( 1000 * (WINNER_TIME / USER_TIME)^3 )

### CSV export format ###
```
teams.csv:
team_id ; raid_id ; team_number_in_raid ; team_name

users.csv:
user_id ; user_name ; birth_year

user_teams.csv:
user_id ; raid_id ; team_id

full.csv:
raid_id ; user_id ; user_name ; birth_year ; team_id ; result_in_sec ; place ; winner_time; team_name ; team_number
```

### Any questions? ###
skype: nikolay.zhariy
email: nzhariy@gmail.com